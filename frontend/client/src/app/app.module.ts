import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ExpenseOperationComponent } from './component/expense-operation/expense-operation.component';
import { CategoryOperationComponent } from './component/category-operation/category-operation.component';
import { ExpensesMonthlyComponent } from './component/expenses-monthly/expenses-monthly.component';
import { CategoriesPercentsComponent } from './component/categories-percents/categories-percents.component';


@NgModule({
  declarations: [
    AppComponent,
    ExpenseOperationComponent,
    CategoryOperationComponent,
    ExpensesMonthlyComponent,
    CategoriesPercentsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
