import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CategoriesPercentsComponent } from './component/categories-percents/categories-percents.component';
import { CategoryOperationComponent } from './component/category-operation/category-operation.component';
import { ExpenseOperationComponent } from './component/expense-operation/expense-operation.component';
import { ExpensesMonthlyComponent } from './component/expenses-monthly/expenses-monthly.component';

const routes: Routes = [
  {path:"expense-operation", component:ExpenseOperationComponent},
  {path:"category-operation", component:CategoryOperationComponent},
  {path:"categories-percents", component:CategoriesPercentsComponent},
  {path:"expenses-monthly",component:ExpensesMonthlyComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
