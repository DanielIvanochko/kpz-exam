import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Category } from '../model/Category';
import { Expense } from '../model/Expense';

@Injectable({
  providedIn: 'root'
})
export class FinancesService {
  private url: string = "https://localhost:7038/Finances";
  private header: HttpHeaders = new HttpHeaders().set('Content-Type','application/json');

  constructor(private http: HttpClient) { }
  addExpense(expense: Expense): Observable<any>{
    return this.http.post<any>(this.url+"/expense", expense, {headers:this.header});
  }
  addCategory(category: Category): Observable<any>{
    return this.http.post<any>(this.url+"/category", category, {headers:this.header});
  }
  updateCategoryInExpense(expenseName: string,category: Category): Observable<any>{
    return this.http.put<any>(this.url+"/category/"+expenseName, category, {headers:this.header});
  }
  deleteExpense(expenseName: string): Observable<any>{
    return this.http.delete<any>(this.url+"/expense/"+expenseName, {headers:this.header});
  }
  getExpensesForEachMonth(): Observable<{ [key: number]: number }>{
    return this.http.get<{ [key: number]: number }>(this.url+"/months", {headers:this.header});
  }
  getCategoriesPercents(): Observable<{ [key: string]: number }>{
    return this.http.get<{ [key: string]: number }>(this.url+"/percents", {headers:this.header});
  }
}
