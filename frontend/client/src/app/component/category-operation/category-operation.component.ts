import { Component, OnInit } from '@angular/core';
import { Category } from 'src/app/model/Category';
import { FinancesService } from 'src/app/service/finances.service';

@Component({
  selector: 'app-category-operation',
  templateUrl: './category-operation.component.html',
  styleUrls: ['./category-operation.component.css']
})
export class CategoryOperationComponent implements OnInit {

  category: Category = {
    Name: ""
  }

  constructor(private service: FinancesService) { }

  ngOnInit(): void {
  }
  addCategory(): void{
    this.service.addCategory(this.category).subscribe(()=>{
      alert('category added')
    },()=>{
      alert('category WAS NOT added')
    })
  }
}
