import { Component, OnInit } from '@angular/core';
import { Category } from 'src/app/model/Category';
import { Expense } from 'src/app/model/Expense';
import { FinancesService } from 'src/app/service/finances.service';

@Component({
  selector: 'app-expense-operation',
  templateUrl: './expense-operation.component.html',
  styleUrls: ['./expense-operation.component.css']
})
export class ExpenseOperationComponent implements OnInit {

  expense: Expense = {
    Name: '',
    Cost: 0,
    Date:'',
    CategoryName: ''
  }
  category: Category = {
    Name: ''
  }
  expenseForUpdate: string = '';
  expenseForDelete: string = '';
  constructor(private service: FinancesService) { }

  ngOnInit(): void {
  }
  addExpense(): void{
    this.service.addExpense(this.expense).subscribe(()=>{
      alert("expense added");
    },()=>{
      alert("expense WAS NOT added");
    });
  }
  updateExpenseCategory(): void{
    this.service.updateCategoryInExpense(this.expenseForUpdate, this.category)
    .subscribe(()=>{
      alert("expense's category updated");
    },()=>{
      alert("expense's category WAS NOT updated");
    });
  }
  deleteExpense(): void{
    this.service.deleteExpense(this.expenseForDelete).subscribe(()=>{
      alert("expense deleted");
    },()=>{
      alert("expense WAS NOT deleted");
    });
  }
}
