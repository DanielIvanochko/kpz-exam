import { Component, OnInit } from '@angular/core';
import { FinancesService } from 'src/app/service/finances.service';

@Component({
  selector: 'app-categories-percents',
  templateUrl: './categories-percents.component.html',
  styleUrls: ['./categories-percents.component.css']
})
export class CategoriesPercentsComponent implements OnInit {

  constructor(private service: FinancesService) { }
  categoriesPercents: {[key:string]:number} = {}
  ngOnInit(): void {
    this.service.getCategoriesPercents().subscribe(c=>this.categoriesPercents = c);
  }
  
}
