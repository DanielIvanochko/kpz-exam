import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoriesPercentsComponent } from './categories-percents.component';

describe('CategoriesPercentsComponent', () => {
  let component: CategoriesPercentsComponent;
  let fixture: ComponentFixture<CategoriesPercentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CategoriesPercentsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CategoriesPercentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
