import { Component, OnInit } from '@angular/core';
import { FinancesService } from 'src/app/service/finances.service';

@Component({
  selector: 'app-expenses-monthly',
  templateUrl: './expenses-monthly.component.html',
  styleUrls: ['./expenses-monthly.component.css']
})
export class ExpensesMonthlyComponent implements OnInit {

  constructor(private service: FinancesService) { }
  expensesMonthly: {[key: number]: number} = {}
  ngOnInit(): void {
    this.service.getExpensesForEachMonth().subscribe(e=>this.expensesMonthly = e);
  }

}
