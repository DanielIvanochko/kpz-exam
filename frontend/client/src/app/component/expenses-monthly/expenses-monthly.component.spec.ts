import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpensesMonthlyComponent } from './expenses-monthly.component';

describe('ExpensesMonthlyComponent', () => {
  let component: ExpensesMonthlyComponent;
  let fixture: ComponentFixture<ExpensesMonthlyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExpensesMonthlyComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ExpensesMonthlyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
