export interface Expense{
    Name: string;
    Cost: number;
    Date: string;
    CategoryName: string;
}