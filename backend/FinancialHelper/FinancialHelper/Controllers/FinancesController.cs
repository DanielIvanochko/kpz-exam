using FinancialHelper.Service;
using FinancialHelper.Viewmodel;
using Microsoft.AspNetCore.Mvc;

namespace FinancialHelper.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FinancesController : ControllerBase
    {
        private FinancesService service;

        public FinancesController()
        {
            service = new FinancesService();
        }

        [HttpPost("expense")]
        public void AddExpense(ExpenseViewModel viewModel)
        {
            service.AddExpense(viewModel);
        }
        [HttpPost("category")]
        public void AddCategory(CategoryViewModel view)
        {
            service.AddCategory(view);
        }
        [HttpPut("category/{expenseName}")]
        public void ChangeCategoryInExpense(string expenseName, CategoryViewModel view)
        {
            service.ChangeCategoryInExpense(expenseName, view);
        }
        [HttpDelete("expense/{expenseName}")]
        public void DeleteExpense(string expenseName)
        {
            service.DeleteExpense(expenseName);
        }
        [HttpGet("months")]
        public Dictionary<int, double> GetExpensesForEachMonth()
        {
            return service.GetExpensesForEachMonth();
        }
        [HttpGet("percents")]
        public Dictionary<String, double> GetCategoriesPercents()
        {
            return service.GetCategoriesPercents();
        }
    }
}