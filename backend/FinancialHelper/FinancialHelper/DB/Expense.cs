﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FinancialHelper.DB
{
    public class Expense
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Cost { get; set; }
        [Column(TypeName = "Date")]
        public DateTime Date { get; set; }
        public int CategoryId { get; set; } 
    }
}
