﻿namespace FinancialHelper.DB
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
