﻿using System.Data.Entity;

namespace FinancialHelper.DB
{
    public class FinancesContext : DbContext
    {
        public DbSet<Category> Categories { get; set; }
        public DbSet<Expense> Expenses { get; set; }
    }
}
