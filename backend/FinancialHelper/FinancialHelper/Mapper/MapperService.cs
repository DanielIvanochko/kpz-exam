﻿using AutoMapper;
using FinancialHelper.DB;
using FinancialHelper.Viewmodel;

namespace FinancialHelper
{
    public class MapperService
    {
        private Mapper mapper;

        public Mapper Mapper { get => mapper; set => mapper = value; }
        public MapperService()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Expense, ExpenseViewModel>();
                cfg.CreateMap<ExpenseViewModel, Expense>();
                cfg.CreateMap<Category, CategoryViewModel>();
                cfg.CreateMap<CategoryViewModel, Category>();
            });
            mapper = new Mapper(config);
        }
    }
}
