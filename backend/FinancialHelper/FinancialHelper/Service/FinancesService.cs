﻿using AutoMapper;
using FinancialHelper.DB;
using FinancialHelper.Viewmodel;
using Microsoft.AspNetCore.Mvc;

namespace FinancialHelper.Service
{
    public class FinancesService
    {
        private Mapper mapper;
        private FinancesContext context;
        public FinancesService()
        {
            mapper = new MapperService().Mapper;
            context = new FinancesContext();
        }
        /*
         1.додавати затрати, категорії
        2. змінювати категорію у затратах
        3.видаляти затрати
        4. подивитися скільки у відсотковому значенні 
         було потрачено на ту
         чи іншу категорію і 
        5. витрати по місяцях.
         */
        public Dictionary<int, double> GetExpensesForEachMonth()
        {
            var expensesForEachMonth = new Dictionary<int, double>();
            var expenses = context.Expenses;
            foreach(var expense in expenses)
            {
                int month = expense.Date.Month;
                if (expensesForEachMonth.ContainsKey(month))
                {
                    expensesForEachMonth[month] += expense.Cost;
                }
                else
                {
                    expensesForEachMonth.Add(month, expense.Cost);
                }
            }
            return expensesForEachMonth;
        }
        public Dictionary<String, double> GetCategoriesPercents()
        {
            Dictionary<String, double> result = new Dictionary<string, double>();
            var expensesForEachCategory = GetExpensesForEachCategory();
            double fullExpenses = GetFullExpenses();
            foreach (var expense in expensesForEachCategory)
            {
                result.Add(expense.Key.Name, expense.Value / fullExpenses);
            }
            return result;
        }
        private Dictionary<Category, double> GetExpensesForEachCategory()
        {
            Dictionary<Category, double> result = new Dictionary<Category, double>();
            var expenses = context.Expenses;
            foreach(var expense in expenses)
            {
                var category = GetCategoryById(expense.CategoryId);
                if (result.ContainsKey(category))
                {
                    result[category] += expense.Cost;
                }
                else
                {
                    result.Add(category, expense.Cost);
                }
            }
            return result;
        }
        private Category GetCategoryById(int id)
        {
            return context.Categories.FirstOrDefault(c => c.Id == id);
        }
        private double GetFullExpenses()
        {
            var expenses = context.Expenses;
            double fullExpenses = 0;
            foreach (var expense in expenses)
            {
                fullExpenses += expense.Cost;
            }
            return fullExpenses;
        }

        public void AddExpense(ExpenseViewModel view)
        {
            Expense expense = mapper.Map<Expense>(view);
            Category category = context.Categories.FirstOrDefault(c => c.Name == view.CategoryName);
            expense.CategoryId = category.Id;
            context.Expenses.Add(expense);
            context.SaveChanges();
        }
        public void AddCategory(CategoryViewModel view)
        {
            Category category = mapper.Map<Category>(view);
            context.Categories.Add(category);
            context.SaveChanges();
        }
        public void ChangeCategoryInExpense(string expenseName, CategoryViewModel view)
        {
            string name = view.Name;
            Category category = context.Categories.FirstOrDefault(c => c.Name == name);
            Expense expense = context.Expenses.FirstOrDefault(e => e.Name == expenseName);
            expense.CategoryId = category.Id;
            context.SaveChanges();
        }
        public void DeleteExpense(string expenseName)
        {
            Expense expense = context.Expenses.FirstOrDefault(e => e.Name == expenseName);
            context.Expenses.Remove(expense);
            context.SaveChanges();
        }
    }
}
