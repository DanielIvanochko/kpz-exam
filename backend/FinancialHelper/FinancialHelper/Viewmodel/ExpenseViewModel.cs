﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FinancialHelper.Viewmodel
{
    public class ExpenseViewModel
    {
        public string Name { get; set; }
        public double Cost { get; set; }
        [Column(TypeName = "Date")]
        public DateTime Date { get; set; }
        public string CategoryName { get; set; }
    }
}
